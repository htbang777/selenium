package test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class SearchGoogle {
	
	private static ExtentTest test;
	private static ExtentReports report;
	private static WebDriver driver;
	
//	@Test
//	public static void main(String[] args) {
//		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("H:\\Document\\Selenium\\ExtentReportResults.html");
//		report = new ExtentReports();
//		report.attachReporter(htmlReporter);
//		test = report.createTest("Search Selenium", "Search for Selenium video");
//		
//		System.setProperty("webdriver.chrome.driver", "H:\\Document\\Selenium\\chromedriver_win32\\chromedriver.exe");
//		WebDriver driver = new ChromeDriver();
//		test.log(Status.INFO, "starting test case");
//		
//		driver.get("https://google.com/");
//		test.log(Status.PASS, "Open google page");
//		
//		WebElement searchElement = driver.findElement(By.name("q"));
//		searchElement.sendKeys("Selenium");
//		test.log(Status.PASS, "Enter key");
//		
//		searchElement.sendKeys(Keys.ENTER);
//		test.log(Status.PASS, "Press enter");
//		
//		driver.close();
//		test.log(Status.PASS, "Close driver");
//		
//		driver.quit();
//		test.log(Status.INFO, "End test");
//		report.flush();
//	}
	
	
	
	@BeforeClass
	public static void startTest() {
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/ExtentReportResults.html");
		report = new ExtentReports();
		report.attachReporter(htmlReporter);
	}
	
	@Rule
    public TestRule watcher = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
			try {
				String screenshotPath = getScreenShot(driver, "fail-screenshot");
				test.fail("Test Case Failed with error: " + e.getMessage() + test.addScreenCaptureFromPath(screenshotPath));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        }
    };
	
	@Test
	public void extentReportsDemo_successful() throws IOException {
		test = report.createTest("Search Selenium success", "Search for Selenium video success");
		System.setProperty("webdriver.chrome.driver", "H:\\Document\\Selenium\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		try {
			driver.get("https://google.com/");
			test.log(Status.PASS, "Open google page");
			
			WebElement searchElement = driver.findElement(By.name("q"));
			searchElement.sendKeys("Selenium");
			test.log(Status.PASS, "Enter key");
			
			searchElement.sendKeys(Keys.ENTER);
			test.log(Status.PASS, "Press enter");
			
		} catch (Exception e) {
			String screenshotPath = getScreenShot(driver, "fail-screenshot");
			test.fail("Test Case Failed with error: " + e.getMessage() + test.addScreenCaptureFromPath(screenshotPath));
		}
	}
	
	@Test
	public void extentReportsDemo_fail() throws IOException {
		test = report.createTest("Search Selenium fail", "Search for Selenium video fail");
		System.setProperty("webdriver.chrome.driver", "H:\\Document\\Selenium\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		try {
			driver.get("https://google.com/");
			test.log(Status.PASS, "Open google page");
			Assert.assertEquals(1, 2);
		} catch (Exception e) {
			String screenshotPath = getScreenShot(driver, "fail-screenshot");
			test.fail("Test Case Failed with error: " + e.getMessage() + test.addScreenCaptureFromPath(screenshotPath));
		}
	}
	
	@AfterClass
	public static void endTest() throws IOException {
		driver.close();
		driver.quit();
		test.log(Status.INFO, "End test");
		report.flush();
	}
	
	
	public static String getScreenShot(WebDriver driver, String screenshotName) throws IOException {
		 String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		 TakesScreenshot ts = (TakesScreenshot) driver;
		 File source = ts.getScreenshotAs(OutputType.FILE);
		  
		 // after execution, you could see a folder "FailedTestsScreenshots" under src folder
		 String destination = System.getProperty("user.dir") + "/reports/" + screenshotName + dateName + ".png";
		 File finalDestination = new File(destination);
		 FileUtils.copyFile(source, finalDestination);
		 return destination;
	}
	
}
